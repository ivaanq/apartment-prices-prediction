apartment_prices_prediction
==============================

This is an educational project within the MLOps course.

There is a data set about apartments in the city of Seattle, Washington. The task is to predict the price of housing according to the available data.
The data has the following columns:
* id — housing identification number
* date — date of sale of the house
* price — price
* bedrooms — number of bedrooms
* bathrooms — the number of bathrooms where .5 means a room with a toilet,
but without a shower
* sqft_living — housing area
* sqft_lot — plot area
* floors — number of floors
* waterfront — is the embankment visible
* view — how good is the view
* condition — index from 1 to 5, responsible for the condition of the apartment
* grade — 1 to 13, 1-3 corresponds to a poor level of construction and design,
3-7 — medium level, 11-13 — high.
* sqft_above — living area above ground level
* sqft_basement — living area below ground level
* yr_built — year of housing construction
* yr_renovated — the year of the last housing renovation
* zipcode — zip code
* lat — latitude
* long — longitude

# CLI

An exaple of script usage:

```
python3 src/data/clean_data.py ./data/raw/houses_train.csv ./data/interim/houses_train_cleaned.csv
```

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   ├── config         <- Configuration files
    │   │   └── config.yaml
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
