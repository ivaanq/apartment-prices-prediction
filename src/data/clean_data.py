# -*- coding: utf-8 -*-
import click
import pandas as pd

MIN_PRICE = 1000
MAX_PRICE = 1e10
MAX_ROOMS = 100


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def clean_data(input_filepath, output_filepath):
    """Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready for feature extraction (saved in ../interim).

    Params:
        input_filepath (str): path of input csv file
        output_filepath (str): path to save cleaned data (csv)
    """
    data = pd.read_csv(input_filepath)

    data = data.dropna()
    data = data[(data["price"] > MIN_PRICE) & (data["price"] < MAX_PRICE)]
    data = data[(data["floors"] >= 1) & (data["floors"] <= MAX_ROOMS)]
    data = data[(data["bedrooms"] >= 1) & (data["bedrooms"] <= MAX_ROOMS)]
    data = data[(data["bathrooms"] >= 1) & (data["bathrooms"] <= MAX_ROOMS)]

    data.to_csv(output_filepath, index=False)


if __name__ == "__main__":
    clean_data()
