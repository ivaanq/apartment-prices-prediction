import click
from catboost import CatBoostRegressor
import yaml
import pandas as pd
import json
import numpy as np
import os
from sklearn.metrics import mean_absolute_error, mean_squared_error
import mlflow


def get_model(config_path):
    with open(config_path, "r") as f:
        config = yaml.load(f, Loader=yaml.loader.SafeLoader)

    return CatBoostRegressor(**config["catboost"], verbose=False)


def get_data(data_path):
    data = pd.read_csv(data_path)
    y = data[["price"]]
    X = data[data.columns[data.columns != "price"]]

    return X, y


def mape(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return 100 * np.mean(np.abs((y_true - y_pred) / y_true))


os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://127.0.0.1:9000"

mlflow.set_tracking_uri("http://127.0.0.1:5000")
mlflow.set_experiment("test_exp")


@click.command()
@click.argument("config_path", type=click.Path(exists=True))
@click.argument("train_path", type=click.Path(exists=True))
@click.argument("test_path", type=click.Path(exists=True))
@click.argument("metric_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def train(config_path, train_path, test_path, metric_path, output_path):
    """Runs training script. model parameters are got from config
    data come from data_path.

    Params:
        config_path (str): path to config (.yaml)
        train_path (str): path to train data (.csv)
        test_path (str): path to test data (.csv)
        metric_path (str): path to save metrics (.json)
        output_path (str): path to save the model (.cbm for catboost)
    """
    model = get_model(config_path)
    X_train, y_train = get_data(train_path)
    model.fit(X_train, y_train)
    model.save_model(output_path)

    X_test, y_test = get_data(test_path)
    y_pred = model.predict(X_test)
    metrics = dict(
        mae=mean_absolute_error(y_test, y_pred),
        rmse=mean_squared_error(y_test, y_pred),
        mape=mape(y_test, y_pred),
    )

    signature = mlflow.models.signature.infer_signature(X_test, y_test)

    mlflow.log_metrics(metrics)
    mlflow.log_artifacts(output_path)
    mlflow.catboost.log_model(
        cb_model=model,
        artifact_path=output_path,
        registered_model_name="test_catboost",
        signature=signature,
    )

    with open(metric_path, "w") as f:
        json.dump(metrics, f)


if __name__ == "__main__":
    train()
