# -*- coding: utf-8 -*-
import click
import pandas as pd


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath_train", type=click.Path())
@click.argument("output_filepath_test", type=click.Path())
def prepate_train_test(input_filepath, output_filepath_train, output_filepath_test):
    """Runs data processing scripts to make features based on cleaned data
    from (../interim). Processed data is saved to (../processed)
    and ready for training.

    Params:
        input_filepath (str): path of input csv file
        output_filepath_train (str): path to save processed train data (csv)
        output_filepath_test (str): path to save processed test data (csv)
    """
    data = pd.read_csv(input_filepath)

    train = data.sample(frac=0.8, random_state=42)
    test = data.drop(train.index)

    train.to_csv(output_filepath_train, index=False)
    test.to_csv(output_filepath_test, index=False)


if __name__ == "__main__":
    prepate_train_test()
