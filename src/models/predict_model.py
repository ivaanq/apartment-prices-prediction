from catboost import CatBoostRegressor
import pandas as pd


def get_model(model_path):
    model = CatBoostRegressor(verbose=False)
    model.load_model(model_path)

    return model


def get_data(data_path):
    data = pd.read_csv(data_path)
    X = data[data.columns[data.columns != "price"]]

    return X


def predict(model_path, data_path):
    """Predicts prices for new data.

    Params:
        model_path (str): path to the model (.cbm for catboost)
        data_path (str): path to data (.csv)

    Returns:
        np.array(N,): prediction of model
    """

    model = get_model(model_path)
    X = get_data(data_path)
    return model.predict(X)
