# -*- coding: utf-8 -*-
import click
import pandas as pd
import numpy as np


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def extract_features(input_filepath, output_filepath):
    """Runs data processing scripts to make features based on cleaned data
    from (../interim). Processed data is saved to (../processed)
    and ready for training.

    Params:
        input_filepath (str): path of input csv file
        output_filepath (str): path to save processed data (csv)
    """
    data = pd.read_csv(input_filepath)
    data = data.drop(["id"], axis=1)

    # let the date be in the format of days
    data["date"] = (
        pd.to_datetime(data["date"]) - pd.to_datetime("19700101")
    ) / np.timedelta64(1, "D")

    # leave only one element associated with the area
    data["sqft_living"] += data["sqft_above"] + data["sqft_basement"]
    data = data.drop(columns=["sqft_above", "sqft_basement"])

    # feature: mean price by zipcode
    zip2price = dict(data["price"].groupby(data["zipcode"]).mean())
    data["price_by_zipcode"] = data["zipcode"].apply(zip2price.get)

    # features: distances to rich and poor houses / zipcodes
    luxurious_zipcode = data["price"].groupby(data["zipcode"]).mean().idxmax()
    borough_zipcode = data["price"].groupby(data["zipcode"]).mean().idxmin()
    luxurious_centroid = (
        data[["lat", "long"]][data["zipcode"] == luxurious_zipcode].mean(axis=0).values
    )
    borough_centroid = (
        data[["lat", "long"]][data["zipcode"] == borough_zipcode].mean(axis=0).values
    )
    best_home = data[["lat", "long"]][data.index == data["price"].argmax()].values
    worst_home = data[["lat", "long"]][data.index == data["price"].argmin()].values

    data["dist_to_luxury"] = np.sqrt(
        ((data[["lat", "long"]] - luxurious_centroid) ** 2).sum(axis=1)
    )
    data["dist_to_borough"] = np.sqrt(
        ((data[["lat", "long"]] - borough_centroid) ** 2).sum(axis=1)
    )
    data["dist_to_best_home"] = np.sqrt(
        ((data[["lat", "long"]] - best_home) ** 2).sum(axis=1)
    )
    data["dist_to_worst_home"] = np.sqrt(
        ((data[["lat", "long"]] - worst_home) ** 2).sum(axis=1)
    )

    # make features normally distributed
    for colname in ["sqft_living", "sqft_lot", "sqft_living15", "sqft_lot15"]:
        data[colname] = np.log(data[colname] + 1)

    data.to_csv(output_filepath, index=False)


if __name__ == "__main__":
    extract_features()
